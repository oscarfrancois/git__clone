# Instructions pour la mise en place d'un certificat de sécurité (SSH) sur un serveur gitlab

Les instructions suivantes ont été conçues et testées entre un environnement git / bash en local (compatible linux, mac et windows) et un dépôt git distant hébergé par la société gitlab.com
Le paramétrage sera sensiblement le même avec toute autre solution (ex: machine virtuelle personnelle, hébergement infomaniak, github, etc).

1) création du jeu de clef pour sécuriser la connexion:
```
cd
mkdir .ssh
cd .ssh
ssh-keygen -t ed25519 -C "MON_ADRESSE_COURRIEL@eduge.ch" -f id_ed25519_gitlab
```

Puis appuyez seulement sur la touche entrée pour les questions qui vous seront posées ("passphrase", etc)

```
ls
```

Votre jeu de clef doit alors apparaître sous la forme de 2 fichiers:
```
id_ed25519_gitlab
id_ed25519_gitlab.pub
```
Pour les curieux, le terme ed25519 indique que l'on utilise un système moderne d'encryption dit elliptique d'une longueur de clef de 128 bits. Un module de sécurité du CFC vous apportera les bases théoriques de sécurité. Pour le moment, considérez que c'est un niveau de sécurité largement suffisant pour nos besoins.
le fichier id_ed25519_gitlab.pub est votre clef publique que vous devez ajouter au menu "SSH Keys" de votre profil gitlab.
le fichier id_ed25519_gitlab est votre clef privée. Elle ne doit jamais être divulguée.

2) Indiquez que ce jeu de clef doit être utilisé pour les dépôts gitlab.com:
Pour cela, vous devez ajouter un fichier ~/.ssh/config
('~' indiquant votre répertoire local personnel)
ayant le contenu suivant:

```
Host gitlab.com
IdentityFile ~/.ssh/id_ed25519_gitlab
```

Astuce: dans la fenêtre git bash, vous pouvez effectuer cette action en copiant/collant les lignes suivantes:
```
cat << EOF >> ~/.ssh/config
Host gitlab.com
IdentityFile ~/.ssh/id_ed25519_gitlab
EOF
```

3) Sur le site gitlab.com, dans le menu Settings -> SSH keys
copiez/collez le contenu du fichier 
```
~/.ssh/id_ed25519_gitlab.pub
```
dans la zone de texte multi-lignes, puis cliquez sur "Add key".

4) Voilà, vous êtes fin prêt pour cloner en local le dépôt du serveur gitlab.
Sur gitlab.com, créez un projet AVEC un accès publique (attention, par défaut, l'accès est privé),
sélectionnez ensuite ce projet. Puis copiez le lien du menu "Clone".

5) sous git bash, vous pouvez maintenant coller l'URL git de votre projet distant en y rajouter avant "git clone".
exemple:
```
git clone git@gitlab.com:MON_NOM_UTILISATEUR_GITLAB/MON_PROJET.git
```
